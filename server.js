const express = require("express");
var app = express();
var bodyParser = require("body-parser");
const accountModel = require("./model/account");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post("/login", (req, res, next) => {
  var username = req.body.username;
  var password = req.body.password;
  accountModel
    .findOne({
      username: username,
      password: password
    })
    .then(function (data) {
        if(data){
            res.json('Login successfully');
        }
        else{
            res.json('Login fail')
        }
    })
    .catch(function (err) {
      res.json(err);
    });
});

app.listen(3000, () => {
  console.log(`Server started on port`);
});
