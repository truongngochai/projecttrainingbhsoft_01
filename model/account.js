const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/Example');

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const account = new Schema({
    // _id: ObjectId,
  username: String,
  password: String
},{
    collection:'Account'
});

const AccountModel = mongoose.model('account', account);

module.exports = AccountModel;

// AccountModel.create({
//     username: 'haitn',
//     password: '123456'
// })
// .then(function(data){
//     console.log(data);
// })
// .catch(function(err){
//     console.log(err);
// })

// AccountModel.find({
// })
// .then(function(data){
//     console.log(data[0].username);
// })
// .catch(function(err){
//     console.log(err);
// })